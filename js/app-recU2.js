// Botón Generar
function generar(){
    var txtEdad = document.getElementById('edad');
    var txtAltura = document.getElementById('altura');
    var txtPeso = document.getElementById('peso');

    //Variables aleatorias
    let edadAleatorio = 0;
    let alturaAleatorio = 0.0;
    let pesoAleatorio = 0.0;

    // Calcular numeros aleatorios en rangos
    edadAleatorio = Math.floor(Math.random() * (99 - 18 + 1)) + 18;
    alturaAleatorio = (Math.random() * (2.5 - 1.5) + 1.5).toFixed(2);
    pesoAleatorio = (Math.random() * (130.0 - 20.0) + 20.0).toFixed(2);

    // Mostrar información
    txtEdad.innerHTML = txtEdad.setAttribute("value", edadAleatorio);
    txtAltura.innerHTML = txtAltura.setAttribute("value", alturaAleatorio);
    txtPeso.innerHTML = txtPeso.setAttribute("value", pesoAleatorio);
}

function calcular(){
    var altura = document.getElementById('altura').value;
    var peso = document.getElementById('peso').value;
    var txtImc = document.getElementById('imc');
    var txtNivel = document.getElementById('nivel');

    //Operaciones
    let calcImc = peso / (altura * altura);
    let calcNivel;

    if(calcImc < 18.5){
        calcNivel = "Bajo peso";
    }else if(calcImc > 18.5 && calcImc < 25){
        calcNivel = "Peso saludable";
    }else if(calcImc >= 25 && calcImc < 30){
        calcNivel = "Sobrepeso";
    }else if(calcImc > 30){
        calcNivel = "Obesidad";
    }

    // Mostrar imc y nivel
    txtImc.innerHTML = txtImc.setAttribute("value", calcImc);
    txtNivel.innerHTML = txtNivel.setAttribute("value", calcNivel);
}

// Variables globales para guardar los registros
var numFinal = 0;
var imcFinal = 0;

function registrar(){
    var altura = document.getElementById('altura').value;
    var peso = document.getElementById('peso').value;
    var edad = document.getElementById('edad').value;

    //Operaciones
    let calcImc = peso / (altura * altura);
    let calcNivel;

    if(calcImc < 18.5){
        calcNivel = "Bajo peso";
    }else if(calcImc > 18.5 && calcImc < 25){
        calcNivel = "Peso saludable";
    }else if(calcImc >= 25 && calcImc < 30){
        calcNivel = "Sobrepeso";
    }else if(calcImc > 30){
        calcNivel = "Obesidad";
    }

    numFinal++;

    //Conseguir labels a llenar
    let lblNum = document.getElementById('lblNum');
    let lblEdad = document.getElementById('lblEdad');
    let lblAltura = document.getElementById('lblAltura');
    let lblPeso = document.getElementById('lblPeso');
    let lblImc = document.getElementById('lblImc');
    let lblNivel = document.getElementById('lblNivel');

    //Mostrar los labels seleccionados
    lblNum.innerHTML = lblNum.innerHTML + numFinal + "<br>";
    lblEdad.innerHTML = lblEdad.innerHTML + edad + "<br>";
    lblAltura.innerHTML = lblAltura.innerHTML + altura + "<br>";
    lblPeso.innerHTML = lblPeso.innerHTML + peso + "<br>";
    lblImc.innerHTML = lblImc.innerHTML + calcImc + "<br>";
    lblNivel.innerHTML = lblNivel.innerHTML + calcNivel + "<br>";

    // Promedio del imc de todos los registro
    imcFinal = imcFinal + calcImc;
    let promedioImc = imcFinal / numFinal;
    
    //Mostrar promedio final
    let lblPromedioImc = document.getElementById('lblPromedioImc');
    lblPromedioImc.innerText = "";
    lblPromedioImc.innerText = lblPromedioImc.innerText + promedioImc; 
}

function borrar(){
    //Conseguir labels a llenar
    let lblNum = document.getElementById('lblNum');
    let lblEdad = document.getElementById('lblEdad');
    let lblAltura = document.getElementById('lblAltura');
    let lblPeso = document.getElementById('lblPeso');
    let lblImc = document.getElementById('lblImc');
    let lblNivel = document.getElementById('lblNivel');
    let lblPromedioImc = document.getElementById('lblPromedioImc');

    //Inputs de la pagina
    var txtEdad = document.getElementById('edad');
    var txtAltura = document.getElementById('altura');
    var txtPeso = document.getElementById('peso');
    var txtImc = document.getElementById('imc');
    var txtNivel = document.getElementById('nivel');

    //Eliminar datos
    txtEdad.innerHTML = txtEdad.setAttribute("value", 0);
    txtAltura.innerHTML = txtAltura.setAttribute("value", 0);
    txtPeso.innerHTML = txtPeso.setAttribute("value", 0);
    txtImc.innerHTML = txtImc.setAttribute("value", 0);
    txtNivel.innerHTML = txtNivel.setAttribute("value", 'sin nivel');

    lblNum.innerHTML = "";
    lblEdad.innerHTML = "";
    lblAltura.innerHTML = "";
    lblPeso.innerHTML = "";
    lblImc.innerHTML = "";
    lblNivel.innerHTML = "";
    lblPromedioImc.innerText = "---";

    numFinal = 0;
    imcFinal = 0;
}